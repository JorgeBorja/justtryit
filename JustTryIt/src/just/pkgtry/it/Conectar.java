
package just.pkgtry.it;

import com.sun.istack.internal.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Conectar {
    Connection conectar=null;
    
    public Connection conexion(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conectar=DriverManager.getConnection("jdbc:mysql://localhost/justtryit","root","");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return conectar;
    }
    
    
 public ResultSet SeleccionarDatos(){
    conectar= conexion();
     Statement st;
     ResultSet rs = null;
     try{
         st = conectar.createStatement();
         rs = st.executeQuery("SELECT * FROM names");
     } catch(SQLException ex){
         System.out.println(ex);
         
     }
     return rs;
}
    
}
